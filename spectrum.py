import math
from matplotlib import pyplot as plt
from scipy.signal import periodogram
import json

def func_k0(k0, t0, t):
    if (t < t0):
        return math.exp(-k0 * t)
    else:
        return math.exp(-k0 * t0)
    
def func_k1(k1, t0, t):
    if ( t == 0):
        return 1
    elif (t == t0):
        return math.pow(4 , -1 * (k1 * t0 * t0) / (math.pi))
    elif (t < t0):
        val =  -1 * (k1 * (4 * t * t0 * math.atanh(t/t0)+ (t*t+t0*t0)*math.log(1-(t*t)/(t0*t0))+2*t*t*math.log(t0/t))) / (2 * math.pi)
        return math.exp(val)
    elif (t > t0):
        a = 2 * t0 * t0 * math.log(t / t0)
        atan = math.atanh(t0 / t)
        b = 4 * t * t0 * atan
        c = (t*t+t0*t0)* math.log(1 - ((t0 * t0)/(t *t)))
        val = -1 * (k1 * (b + a +  c)) / (2 * math.pi)
        return math.exp(val)
    else:
        return 0
    
w_res = 1e3
w_max = 50e6
span = 4e6

def spectrum(k0, k1, printFlag=False):
    t_data = []
    counter = 0
    x = 0
    t_end = 1 / w_res
    dt = 1 / w_max
    t0 = 1.77e-6
    while x <= t_end:
        counter += 1
        t_data.append(func_k0(k0,t0,x) * func_k1(k1, t0, x))
        x += dt
    

    t_data_b = t_data.copy()
    t_data_b.reverse()
    t_data.pop(0)
    for val in t_data:
        t_data_b.append(val)

    plt.plot(t_data_b)
    plt.show()
    _ , per = periodogram(t_data_b, window='flattop', return_onesided=False)
    print(len(per))

    plt.plot(per)
    plt.show()

    spec = []
    for val in per:
        spec.append(10 * math.log10(1e-6 * math.sqrt(val) / 1e-3))

    for i in range(int(0.5 * len(spec)) + 1):
        spec.append(spec.pop(0))

    print(len(spec))
    plt.plot(spec)
    plt.show()

    for i in range(42000):
        spec.pop(0)
        spec.pop(-1)
    spec.pop(0)
    plt.plot(spec)
    plt.show()
    
    
s_file = open("spectra.json", "r")
p_file = open("params.json", "r")

spectra = json.load(s_file)
params = json.load(p_file)

s_file.close()
p_file.close()

samples = [1275]

for val in samples:
    print(params[val][0])
    print(params[val][1])
    spectrum(params[val][0], params[val][1])
    plt.plot(spectra[val])
    plt.show()

# spectrum(5e4,10e10)